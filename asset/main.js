function showBack() {
    var num1Elements = document.getElementById("txt-so-thu-nhat").value * 1;
    var num2Elements = document.getElementById("txt-so-thu-hai").value * 1;
    var num3Elements = document.getElementById("txt-so-thu-ba").value * 1;
    var ngay = num1Elements;
    var thang = num2Elements;
    var nam = num3Elements;

    if (num1Elements) {
        ngay = num1Elements - 1;
        document.getElementById(
            "content"
        ).innerHTML = `${ngay} / ${thang} / ${nam}`;
    } else {}
}

function showNext() {
    var num1Elements = document.getElementById("txt-so-thu-nhat").value * 1;
    var num2Elements = document.getElementById("txt-so-thu-hai").value * 1;
    var num3Elements = document.getElementById("txt-so-thu-ba").value * 1;
    var ngay = num1Elements;
    var thang = num2Elements;
    var nam = num3Elements;

    if (num1Elements) {
        ngay = num1Elements + 1;
        document.getElementById(
            "content"
        ).innerHTML = `${ngay} / ${thang} / ${nam}`;
    } else if (
        (num1Elements == 30 && num2Elements == 4) ||
        num3Elements == 6 ||
        num2Elements == 9 ||
        num2Elements == 11
    ) {
        ngày = 1;
        thang = num2Elements + 1;
        document.getElementById(
            "content"
        ).innerHTML = `${ngay} / ${thang} / ${nam}`;
    } else if (num1Elements == 28 && num2Elements == 2) {
        ngày = 1;
        thang = num2Elements + 1;
        document.getElementById(
            "content"
        ).innerHTML = `${ngay} / ${thang} / ${nam}`;
    }
}

document.getElementById("btn-show").addEventListener("click", function() {
    var numberElement = document.getElementById("txt-number").value * 1;

    var hundred = Math.floor(numberElement / 100);
    var ten = Math.floor((numberElement % 100) / 10);
    var unit = Math.floor(numberElement % 10);
    var content = "";
    var contentTen = "";
    var contentUnit = "";

    switch (hundred) {
        case 0:
            {
                content = "";
                break;
            }
        case 1:
            {
                content = "Một trăm";
                break;
            }
        case 2:
            {
                content = "Hai trăm";
                break;
            }
        case 3:
            {
                content = "Ba trăm";
                break;
            }
        case 4:
            {
                content = "Bốn trăm";
                break;
            }
        case 5:
            {
                content = "Năm trăm";
                break;
            }
        case 6:
            {
                content = "Sáu trăm";
                break;
            }
        case 7:
            {
                content = "Bảy trăm";
                break;
            }
        case 8:
            {
                content = "Tám trăm";
                break;
            }
        case 9:
            {
                content = "Chín trăm";
                break;
            }
    }

    switch (ten) {
        case 0:
            {
                contentTen = "Không";
                break;
            }
        case 1:
            {
                contentTen = "Mười";
                break;
            }
        case 2:
            {
                contentTen = "Hai mươi";
                break;
            }
        case 3:
            {
                contentTen = "Ba mươi";
                break;
            }
        case 4:
            {
                contentTen = "Bốn mươi";
                break;
            }
        case 5:
            {
                contentTen = "Năm mươi";
                break;
            }
        case 6:
            {
                contentTen = "Sáu mươi";
                break;
            }
        case 7:
            {
                contentTen = "Bảy mươi";
                break;
            }
        case 8:
            {
                contentTen = "Tám mươi";
                break;
            }
        case 9:
            {
                contentTen = "Chín mươi";
                break;
            }
    }
    switch (unit) {
        case 0:
            {
                contentUnit = "";
                break;
            }
        case 1:
            {
                contentUnit = "Một";
                break;
            }
        case 2:
            {
                contentUnit = "Hai";
                break;
            }
        case 3:
            {
                contentUnit = "Ba";
                break;
            }
        case 4:
            {
                contentUnit = "Bốn";
                break;
            }
        case 5:
            {
                contentUnit = "Năm";
                break;
            }
        case 6:
            {
                contentUnit = "Sáu";
                break;
            }
        case 7:
            {
                contentUnit = "Bảy";
                break;
            }
        case 8:
            {
                contentUnit = "Tám";
                break;
            }
        case 9:
            {
                contentUnit = "Chín";
                break;
            }
    }
    document.getElementById(
        "result"
    ).innerHTML = `${content} ${contentTen} ${contentUnit}`;
});